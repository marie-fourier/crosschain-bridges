## Техническое задание на неделю 5 (мост)

Написать контракт кроссчейн моста для отправки токенов стандарта ERC-20 между сетями Ethereum и Binance Smart chain.
- Написать контракт Bridge
- Написать полноценные тесты к контракту
- Написать скрипт деплоя
- Задеплоить в тестовую сеть
- Написать таск на swap, redeem
- Верифицировать контракт

Требования:
- Функция swap(): списывает токены с пользователя и испускает event ‘swapInitialized’
- Функция redeem(): вызывает функцию ecrecover и восстанавливает по хэшированному сообщению и сигнатуре адрес валидатора, если адрес совпадает с адресом указанным на контракте моста то пользователю отправляются токены
- Функция updateChainById(): добавить блокчейн или удалить по его chainID
- Функция includeToken(): добавить токен для передачи его в другую сеть
- Функция excludeToken(): исключить токен для передачи

## Ссылки:

Презентация https://docs.google.com/presentation/d/14CMVKUrRNP64kbsBbpz_OU3135k_fJSd3YBI1RR__CA/edit?usp=sharing

ECDSA
https://docs.openzeppelin.com/contracts/4.x/api/utils#ECDSA

Signing Messages
https://docs.ethers.io/v4/cookbook-signing.html?highlight=signmessage

Mathematical and Cryptographic Functions
https://docs.soliditylang.org/en/v0.8.0/units-and-global-variables.html#mathematical-and-cryptographic-functions

Дедлайн 25.03.2022

## Instruction
- deploy ERC20 tokens by running

  `npx hardhat run scripts/deploy-erc20.ts --network rinkeby`

  and

  `npx hardhat run scripts/deploy-erc20.ts --network bsc`

- save their contract addreses in .ENV (`ETH_TOKEN` and `BSC_TOKEN`)
- deploy bridges to rinkeby and bsc network by running

  `npx hardhat run scripts/deploy.ts --network rinkeby`

  and

  `npx hardhat run scripts/deploy.ts --network bsc`
- save their addresses in .ENV (`ETH_BRIDGE` and `BSC_BRIDGE`)
- run bridge listener

  `npx hardhat run scripts/eth-bridge.ts --network rinkeby`

  `npx hardhat run scripts/bsc-bridge.ts --network bsc`

## Example
ERC20 on BSC: [0x78D3B0D68abc479891fC618C9D41992b76C4c30f](https://testnet.bscscan.com/address/0x78D3B0D68abc479891fC618C9D41992b76C4c30f)

ERC20 on Rinkeby: [0x7a953EC7850b839BA129C6420ddB18681cfb7a0e](https://rinkeby.etherscan.io/address/0x7a953EC7850b839BA129C6420ddB18681cfb7a0e)

Bridge on BSC: [0xc1af69a8E3AD2be2b598dE1D04cC642d618a17f4](https://testnet.bscscan.com/address/0xc1af69a8E3AD2be2b598dE1D04cC642d618a17f4)

Bridge on Rinkeby: [0x06Aa0b3654bE5aF669A494dda940Fc63FEA4DB3E](https://rinkeby.etherscan.io/address/0x06Aa0b3654bE5aF669A494dda940Fc63FEA4DB3E)
