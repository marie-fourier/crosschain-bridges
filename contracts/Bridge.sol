// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

contract Bridge is Ownable {
  using ECDSA for bytes32;

  /**
   * @dev Emitted when sender wants to swap `_value` amount of `_tokenFrom`
   * from the current chain to `_tokenTo` in `_toChain` chain
   * and send it `_to` wallet
   */
  event Swap(
    uint256 indexed _toChain,
    address _tokenFrom,
    address _tokenTo,
    address indexed _from,
    address indexed _to,
    uint256 _value,
    uint256 _nonce
  );

  /**
   * @dev Emitted when sender receives `_value` amount of `_token`
   * from another chain
   */
  event Redeem(
    uint256 indexed _fromChain,
    address indexed _token,
    address indexed _to,
    uint256 _nonce,
    uint256 _value
  );

  /**
   * @dev Emitted when `_chainId` status changes
   * 1 - enabled
   * 0 - disabled
   */
  event ChainStatusChange(
    uint256 _chainId,
    uint256 _status
  );

  /**
   * @dev Emitted when `_tokenFrom` in the current chain
   * and it's corresponding token `_tokenTo` in `_chainId` chain
   * included
   */
  event IncludeToken(
    uint256 _chainId,
    address _tokenFrom,
    address _tokenTo
  );

  /**
   * @dev Emitted when `_token` is excluded from `_chainId`
   */
  event ExcludeToken(
    uint256 _chainId,
    address _token
  );

  uint256 internal _chainId;
  address internal _bridgeOwner;

  struct Chain {
    uint256 enabled;
    mapping(address => address) tokens;
  }

  mapping(uint256 => uint256) internal _nonces;
  mapping(uint256 => mapping(uint256 => bool)) internal _usedNonces;
  mapping(uint256 => Chain) internal _chains;

  constructor(uint256 chainId, address bridgeOwner) {
    _chainId = chainId;
    _bridgeOwner = bridgeOwner;
  }

  /**
   * @dev Transfers {value} amount of {token} to the pool
   *
   * Requirements:
   * - `chainId` should be enabled
   * - `token` should be included to `chainId`
   *
   * Emits a {Swap} event
   */
  function swap(
    uint256 chainId,
    address token,
    address recipient,
    uint256 value
  ) external {
    Chain storage chain = _chains[chainId];
    require(chain.enabled != 0, "Unknown chain");
    require(chain.tokens[token] != address(0), "Unknown token");

    _nonces[chainId]++;

    emit Swap(
      chainId,
      token,
      chain.tokens[token],
      msg.sender,
      recipient,
      value,
      _nonces[chainId]
    );

    IERC20 erc20 = IERC20(token);
    require(
      erc20.transferFrom(msg.sender, address(this), value),
      "Transfer failed"
    );
  }

  /**
   * @dev Transfers {value} amount of {token} to the sender
   * if a `signature` is signed by the bridge owner
   *
   * Requirements:
   *  must provide unique pair of `fromChain` and `nonce`
   *  to prevent using one signature twice
   *
   * Emits a {Redeem} event
   */
  function redeem(
    uint256 fromChain,
    address token,
    uint256 value,
    uint256 nonce,
    bytes memory signature
  ) external {
    require(_chains[fromChain].enabled != 0, "Unknown chain");
    require(!_usedNonces[fromChain][nonce], "Invalid nonce");
    require(
      keccak256(
        abi.encodePacked(
          fromChain,
          _chainId,
          token,
          value,
          msg.sender,
          nonce
        )
      )
        .toEthSignedMessageHash()
        .recover(signature) == _bridgeOwner,
      "Invalid signature"
    );

    _usedNonces[fromChain][nonce] = true;

    emit Redeem(fromChain, token, msg.sender, nonce, value);

    IERC20 erc20 = IERC20(token);
    require(erc20.transfer(msg.sender, value), "Not enough liquidity");
  }

  /**
   * @dev 
   * emits a {ChainStatusChange} event
   */
  function updateChainById(uint256 chainId) external onlyOwner {
    _chains[chainId].enabled = _chains[chainId].enabled > 0 ? 0 : 1;
    emit ChainStatusChange(chainId, _chains[chainId].enabled);
  }

  /**
   * @dev 
   * emits a {IncludeToken} event
   */
  function includeToken(uint256 chainId, address tokenFrom, address tokenTo) external onlyOwner {
     require(_chains[chainId].enabled != 0, "Unknown chain");
     emit IncludeToken(chainId, tokenFrom, tokenTo);
     _chains[chainId].tokens[tokenFrom] = tokenTo;
  }

  /**
   * @dev 
   * emits a {ExcludeToken} event
   */
  function excludeToken(uint256 chainId, address token) external onlyOwner {
    require(_chains[chainId].enabled != 0, "Unknown chain");
    emit ExcludeToken(chainId, token);
    _chains[chainId].tokens[token] = address(0);
  }
}
