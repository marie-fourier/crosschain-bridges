import { ethers } from "ethers";
require("dotenv").config();

const _decimals = 18;

export const contractAddress = String(process.env.CONTRACT_ADDRESS);

export const getContract = async (hre: any) => {
  const network = await hre.ethers.provider.getNetwork();
  let contractAddress: string;
  if (network.name === "rinkeby") {
    contractAddress = String(process.env.ETH_BRIDGE);
  } else if (network.name === "bnbt") {
    contractAddress = String(process.env.BSC_BRIDGE);
  } else {
    throw new Error("Unknown network");
  }
  return await hre.ethers.getContractAt("Bridge", contractAddress);
};

export const getTokenContract = async (hre: any) => {
  const network = await hre.ethers.provider.getNetwork();
  let contractAddress: string;
  if (network.name === "rinkeby") {
    contractAddress = String(process.env.ETH_TOKEN);
  } else if (network.name === "bnbt") {
    contractAddress = String(process.env.BSC_TOKEN);
  } else {
    throw new Error("Unknown network");
  }
  return await hre.ethers.getContractAt("IERC20", contractAddress);
};

export const getAccounts = async (hre: any) => {
  return await hre.ethers.getSigners();
};

export const parseToken = (value: string) => {
  return ethers.BigNumber.from(value).mul(10 ** _decimals);
};

export const getTimestamp = async (hre: any) => {
  const blockNumber = await hre.ethers.provider.getBlockNumber();
  const block = await hre.ethers.provider.getBlock(blockNumber);
  return block.timestamp;
};
