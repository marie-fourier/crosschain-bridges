import { ethers, config } from "hardhat";
require("dotenv").config();

async function main() {
  console.log(config.networks);
  const network = await ethers.provider.getNetwork();
  const [owner] = await ethers.getSigners();
  const Contract = await ethers.getContractFactory("Bridge");
  const contract = await Contract.deploy(network.chainId, owner.address);

  await contract.deployed();

  console.log(
    `Contract deployed to ${network.name} network at ${contract.address}`
  );

  if (process.env.BSC_TOKEN && process.env.ETH_TOKEN) {
    const { BSC_TOKEN, ETH_TOKEN } = process.env;
    if (network.name === "rinkeby") {
      const { chainId } = config.networks.bsc;
      await contract.updateChainById(Number(chainId));
      await contract.includeToken(Number(chainId), ETH_TOKEN, BSC_TOKEN);
      const token = await ethers.getContractAt("IERC20", ETH_TOKEN);
      await token.approve(contract.address, ethers.utils.parseEther("1000000"));
      await token.transfer(
        contract.address,
        ethers.utils.parseEther("1000000")
      );
      console.log("Enabled bsc chain in eth bridge and added liquidity");
    }
    if (network.name === "bnbt") {
      const { chainId } = config.networks.rinkeby;
      await contract.updateChainById(Number(chainId));
      await contract.includeToken(Number(chainId), BSC_TOKEN, ETH_TOKEN);
      const token = await ethers.getContractAt("IERC20", BSC_TOKEN);
      await token.approve(contract.address, ethers.utils.parseEther("1000000"));
      await token.transfer(
        contract.address,
        ethers.utils.parseEther("1000000")
      );
      console.log("Enabled eth chain in bsc bridge and added liquidity");
    }
  }
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
