import { ethers } from "hardhat";
require("dotenv").config();

async function main() {
  if (!process.env.ETH_BRIDGE) {
    throw new Error("Provide eth bridge contract address");
  }

  const Bridge = await ethers.getContractFactory("Bridge");
  const network = await ethers.provider.getNetwork();
  const [owner] = await ethers.getSigners();
  const bridge = await Bridge.attach(process.env.ETH_BRIDGE);

  bridge.on(
    "Swap",
    async (toChain, tokenFrom, tokenTo, from, to, value, nonce) => {
      const hash = ethers.utils.solidityKeccak256(
        ["uint256", "uint256", "address", "uint256", "address", "uint256"],
        [network.chainId, toChain, tokenTo, value, to, nonce]
      );
      const signature = await owner.signMessage(ethers.utils.arrayify(hash));
      console.log(
        `Tx from: ${from}. Swapping ${value} of ${tokenFrom}. Nonce ${nonce}`
      );
      console.log(`Signature: ${signature}`);
    }
  );

  console.log("Listening to events");
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
