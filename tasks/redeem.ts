import { task } from "hardhat/config";
import { getContract } from "../helpers";

interface TaskArgs {
  chain: Number;
  token: string;
  value: Number;
  nonce: Number;
  signature: string;
}

task("redeem")
  .addParam("chain", "Chain id")
  .addParam("token", "Token address")
  .addParam("value", "Amount of tokens")
  .addParam("nonce", "Nonce")
  .addParam("signature", "Signature")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    try {
      const value = hre.ethers.utils.parseEther(taskArgs.value.toString());
      await contract.redeem(
        taskArgs.chain,
        taskArgs.token,
        value,
        taskArgs.nonce,
        taskArgs.signature
      );
      console.log("Successfully redeemed");
    } catch (err) {
      console.log("Error:", err);
    }
  });
