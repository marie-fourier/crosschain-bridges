import { task } from "hardhat/config";
import { getContract, getTokenContract } from "../helpers";

interface TaskArgs {
  chain: Number;
  token: string;
  recipient: string;
  value: Number;
}

task("swap")
  .addParam("chain", "Chain id")
  .addParam("token", "Token address")
  .addParam("recipient", "Recipient")
  .addParam("value", "Amount of tokens")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const contract = await getContract(hre);
    const token = await getTokenContract(hre);
    try {
      const value = hre.ethers.utils.parseEther(taskArgs.value.toString());
      await token.approve(contract.address, value);
      await contract.swap(
        taskArgs.chain,
        taskArgs.token,
        taskArgs.recipient,
        value
      );
      console.log("Swapped. Ask bridge owner for signature");
    } catch (err) {
      console.log("Error:", err);
    }
  });
