import { expect } from "chai";
import { ethers } from "hardhat";

async function getTimestamp() {
  const blockNumber = await ethers.provider.getBlockNumber();
  const block = await ethers.provider.getBlock(blockNumber);
  return block.timestamp;
}

async function increaseTime(seconds: number) {
  const timestamp = await getTimestamp();
  await setTimestamp(timestamp + seconds);
}

async function setTimestamp(timestamp: number) {
  await ethers.provider.send("evm_mine", [timestamp]);
}

describe("Bridge", async () => {
  let owner: any,
    account1: any,
    ethBridge: any,
    bscBridge: any,
    ethToken: any,
    bscToken: any;

  before(async () => {
    const Contract = await ethers.getContractFactory("Bridge");
    const Token = await ethers.getContractFactory("Token");
    [owner, account1] = await ethers.getSigners();

    ethToken = await Token.deploy();
    await ethToken.deployed();

    bscToken = await Token.deploy();
    await bscToken.deployed();

    ethBridge = await Contract.deploy(1, owner.address);
    await ethBridge.deployed();

    bscBridge = await Contract.deploy(2, owner.address);
    await bscBridge.deployed();

    await bscToken.transfer(
      bscBridge.address,
      ethers.utils.parseEther("1000000")
    );
  });

  it("Swap, redeem, including and excluding token should fail for unsupported chains", async () => {
    await expect(
      ethBridge.swap(2, ethToken.address, account1.address, 20)
    ).to.be.revertedWith("Unknown chain");
    await expect(
      ethBridge.includeToken(2, ethToken.address, bscToken.address)
    ).to.be.revertedWith("Unknown chain");
    await expect(
      ethBridge.excludeToken(2, ethToken.address)
    ).to.be.revertedWith("Unknown chain");
    await expect(
      ethBridge.redeem(3, bscToken.address, 1000, 1, bscToken.address)
    ).to.be.revertedWith("Unknown chain");
  });

  it("Updating chainId should emit event and only owner can use it", async () => {
    await expect(ethBridge.connect(account1).updateChainById(2)).to.be.reverted;
    await expect(ethBridge.updateChainById(2))
      .to.emit(ethBridge, "ChainStatusChange")
      .withArgs(2, 1);
    await expect(ethBridge.updateChainById(2))
      .to.emit(ethBridge, "ChainStatusChange")
      .withArgs(2, 0);
    await expect(ethBridge.updateChainById(2))
      .to.emit(ethBridge, "ChainStatusChange")
      .withArgs(2, 1);
    await expect(bscBridge.updateChainById(1))
      .to.emit(bscBridge, "ChainStatusChange")
      .withArgs(1, 1);
  });

  it("Swap should fail for unsupported tokens", async () => {
    await expect(
      ethBridge.swap(2, bscToken.address, account1.address, 20)
    ).to.be.revertedWith("Unknown token");
  });

  it("Excluding token should emit event and only owner can use it", async () => {
    await expect(ethBridge.connect(account1).excludeToken(2, ethToken.address))
      .to.be.reverted;
    await expect(ethBridge.excludeToken(2, ethToken.address))
      .to.emit(ethBridge, "ExcludeToken")
      .withArgs(2, ethToken.address);
  });

  it("Including token should emit event and only owner can use it", async () => {
    await expect(
      ethBridge
        .connect(account1)
        .includeToken(2, ethToken.address, bscToken.address)
    ).to.be.reverted;
    await expect(ethBridge.includeToken(2, ethToken.address, bscToken.address))
      .to.emit(ethBridge, "IncludeToken")
      .withArgs(2, ethToken.address, bscToken.address);
  });

  it("Swap should fail if transfer is not approved", async () => {
    await expect(
      ethBridge.swap(
        2,
        ethToken.address,
        account1.address,
        ethers.utils.parseEther("10000")
      )
    ).to.be.revertedWith("ERC20: insufficient allowance");
  });

  it("Swap should transfer tokens and emit event", async () => {
    const initialBalance = await ethToken.balanceOf(ethBridge.address);
    ethToken.approve(ethBridge.address, ethers.utils.parseEther("10000"));
    await expect(
      ethBridge.swap(
        2,
        ethToken.address,
        account1.address,
        ethers.utils.parseEther("10000")
      )
    )
      .to.emit(ethBridge, "Swap")
      .withArgs(
        2,
        ethToken.address,
        bscToken.address,
        owner.address,
        account1.address,
        ethers.utils.parseEther("10000"),
        1
      );
    expect(await ethToken.balanceOf(ethBridge.address)).to.eq(
      ethers.utils.parseEther("10000").add(initialBalance)
    );
  });

  it("Redeem should only accept bridge owner's signatures once", async () => {
    const hash = ethers.utils.solidityKeccak256(
      ["uint256", "uint256", "address", "uint256", "address", "uint256"],
      [
        1,
        2,
        bscToken.address.toLowerCase(),
        ethers.utils.parseEther("10000"),
        account1.address.toLowerCase(),
        1,
      ]
    );
    const signature = await owner.signMessage(ethers.utils.arrayify(hash));
    const signature2 = await account1.signMessage(ethers.utils.arrayify(hash));
    await expect(
      bscBridge.redeem(
        1,
        bscToken.address,
        ethers.utils.parseEther("10000"),
        1,
        signature2
      )
    ).to.be.revertedWith("Invalid signature");
    await expect(
      bscBridge
        .connect(account1)
        .redeem(
          1,
          bscToken.address,
          ethers.utils.parseEther("10000"),
          1,
          signature
        )
    )
      .to.emit(bscBridge, "Redeem")
      .withArgs(
        1,
        bscToken.address,
        account1.address,
        1,
        ethers.utils.parseEther("10000")
      );
    expect(await bscToken.balanceOf(account1.address)).to.eq(
      ethers.utils.parseEther("10000")
    );

    await expect(
      bscBridge
        .connect(account1)
        .redeem(
          1,
          bscToken.address,
          ethers.utils.parseEther("10000"),
          1,
          signature
        )
    ).to.be.revertedWith("Invalid nonce");
  });

  it("Redeem tx fails if bridge has not enough liquidity", async () => {
    const hash = ethers.utils.solidityKeccak256(
      ["uint256", "uint256", "address", "uint256", "address", "uint256"],
      [
        1,
        2,
        bscToken.address.toLowerCase(),
        ethers.utils.parseEther("100000000"),
        account1.address.toLowerCase(),
        2,
      ]
    );
    const signature = await owner.signMessage(ethers.utils.arrayify(hash));
    await expect(
      bscBridge
        .connect(account1)
        .redeem(
          1,
          bscToken.address,
          ethers.utils.parseEther("100000000"),
          2,
          signature
        )
    ).to.be.revertedWith("ERC20: transfer amount exceeds balance");
  });
});
